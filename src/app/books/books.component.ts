import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { BookService } from '../book.service';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books: any[];
  book;
  author;

  constructor(public authService: AuthService, private router:Router, private db:AngularFireDatabase,private bookService: BookService) { this.showBook(); }
 
  ngOnInit() {
    this.authService.user.subscribe(user=>{
   
      this.db.list('/users/' + user.uid + '/books').snapshotChanges().subscribe(
        books =>{
          this.books = [];
          books.forEach(
            book => {
              let y = book.payload.toJSON();
              y["$key"] = book.key;
              this.books.push(y);
            }
          )
        }
      )            
    });       
  }

  showBook()
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/uid/' + user.uid + '/books').snapshotChanges().subscribe(
        books => {
          this.books = [];
          books.forEach(
            book => {
              let y = book.payload.toJSON();
              y["$key"] = book.key;
              this.books.push(y);
            }
          )
        }  
      )
    })
  }
  addBook(){
    this.bookService.addBook(this.book,this.author);
    this.book = '';
    this.author=''
    this.showBook();
  }

}
