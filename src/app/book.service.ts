import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class BookService {



  addBook(book:string, author:string)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/uid/'+user.uid+'/books').push({'book':book,'author':author, 'read':false});
    })
  }

  updateBook(key:string, book:string,author:string, read:boolean)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/uid/'+user.uid+'/books').update(key,{'book':book,'author':author, 'read':read});
    })
  }

  updateRead(key:string, book:string, read:boolean)
  {
    this.authService.user.subscribe(user =>{
      this.db.list('/users/uid/'+user.uid+'/books').update(key,{'book':book,'read':read});
    })
    
  }

  constructor(private authService: AuthService,
              private db: AngularFireDatabase)  { }
}