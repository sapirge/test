import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email: string;
  name: string;
  nickname: string;
  password: string;
  passwordAgain: string;
  code: string;
  message: string;
  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit() {
  }


  signUp() {
    this.authService.signUp(this.email, this.password)
    .then(value => {
      this.authService.updateProfile(value.user, this.name);

      this.authService.addUser(value.user, this.name, this.email, this.nickname);
    }).then(value => {
      this.router.navigate(['/books']);
    })    .catch(err => {
      this.code = err.code;
      this.message = err.message;
      console.log(err);
    })
  }
}
