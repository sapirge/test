import { Component, OnInit ,Input} from '@angular/core';
import { BookService } from '../book.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  book;
  author;
  
  tempText;
  temText;
  key;
  read: boolean;
  
  @Input() data:any;

  showButton = false;
  showEditField = false;
  nameSpan=false;
  
  showEdit()
  {
    this.tempText = this.book;
    this.temText=this.author
    this.nameSpan = false;
    this.showEditField = true;
  }
  cancel()
  {
    this.book = this.tempText;
    this.author=this.temText;
    this.showEditField = false;
  }

  save()
  {
    this.bookService.updateBook(this.key,this.book,this.author, this.read);
    this.showEditField = false;
  }
  checkChange()
  {
    this.bookService.updateRead(this.key,this.book,this.read);
  }
  buttonOn() {
    this.nameSpan=true;
    
  }
 
  buttonOff() {
    this.nameSpan = false;
    
  }
  
  constructor(private bookService: BookService,private firebaseAuth: AngularFireAuth) { }

  ngOnInit() {
    this.book = this.data.book;
    this.key = this.data.$key;
    this.read = this.data.read;
    this.author = this.data.author;
  }
}